// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website.
//Execute a function to Sort all the car model names into alphabetical order 
//and log the results in the console as it was returned.


const problem3=(inventory)=>{
    var model=[];
    for(let i=0;i<inventory.length;i++){
        let flag=0;
        for(let j=0;j<model.length;j++){
            if(inventory[i].car_model===model[j]){
                flag=1;
                break;
            }
        }
        if(flag==0){
            model.push(inventory[i].car_model);
        }
    }
    model.sort();
    return model;
}
export default problem3;

