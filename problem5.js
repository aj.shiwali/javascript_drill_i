// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. 
//Using the array you just obtained from the previous problem, 
//find out how many cars were made before the year 2000 and return the array 
//of older cars and log its length.

const problem5=(inventory,modelName)=>{
    let Car=[];
    let i=0;
    for(let j=0;j<modelName.length;j++){
        if(modelName[j]<2000){
            Car.push(inventory[j].car_model);
            i++;
        }
    }
    return {Car,i};
}
export default problem5;

